import { DestinoViaje } from './destino-viaje.model';
import { Observable } from 'rxjs';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.model';
import { Store } from '@ngrx/store';
import { AppState, APP_CONFIG, AppConfig, MyDatabase, db } from './../app.module';
import { HttpRequest, HttpHeaders, HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';
import { forwardRef, Inject, Injectable } from '@angular/core';

@Injectable()
export class DestinosApiClient {
    destinosObs: Observable<DestinoViaje[]>

    constructor(public store: Store<AppState>, @Inject(forwardRef(() => APP_CONFIG)) public config: AppConfig, public http: HttpClient) {
        this.destinosObs = this.store.select<DestinoViaje[]>((state: AppState) => state.destinos.items);
    }

    add(d: DestinoViaje): void {
        const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
        const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if (data.status === 200) {
                this.store.dispatch(new NuevoDestinoAction(d));
                const myDb = db;
                myDb.destinos.add(d);
                console.log('todos los destinos de la db!');
                myDb.destinos.toArray().then(destinos => console.log(destinos))
            }
        });
    }

    getAll(): Observable<DestinoViaje[]> {
        return this.destinosObs;
    }

    elegir(d: DestinoViaje) {
        this.store.dispatch(new ElegidoFavoritoAction(d));
    }

    isSelected(destino: DestinoViaje): Observable<boolean> {
        return this.store.select<boolean>((state: AppState) => state.destinos.favorito == destino);
    }

    getById(id: string): Observable<DestinoViaje> {
        return this.store.select<DestinoViaje>((state: AppState) => state.destinos.items.find(item => item.id == id));
    }
}

@Injectable()
export class DestinosApiClientDecorated extends DestinosApiClient {
    constructor(public store: Store<AppState>, @Inject(forwardRef(() => APP_CONFIG)) public config: AppConfig, public http: HttpClient) {
        super(store, config, http);
    }

    getById(id: string): Observable<DestinoViaje> {
        console.log('llamando por la clase decorada!');
        console.log('config: ' + this.config.apiEndpoint);
        return super.getById(id);
    }
}

@Injectable()
export class DestinosApiClientViejo {
    getById(id: String): Observable<DestinoViaje> {
        console.log('llamando por la clase vieja!');
        return null;
    }
}