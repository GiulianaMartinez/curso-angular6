export class DestinoViaje {
    static _max_id: number = 0;
    public servicios: string[];
    public id: string;

    constructor(public nombre: string, public url: string, public votes: number = 0) {
        this.servicios = ['pileta', 'desayuno'];
        this.id = (DestinoViaje._max_id++).toString();
    }

    voteUp() {
        this.votes++;
    }

    voteDown() {
        this.votes--;
    }

    voteReset() {
        this.votes = 0;
    }
}